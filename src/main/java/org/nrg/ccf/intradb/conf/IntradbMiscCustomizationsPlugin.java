package org.nrg.ccf.intradb.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbMiscCustomizationsPlugin",
			name = "Intradb Miscellaneous Customizations Plugin"
		)
@ComponentScan({ 
	// None, for now
	})
public class IntradbMiscCustomizationsPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbMiscCustomizationsPlugin.class);

	/**
	 * Instantiates a new Intradb Misc Customizations plugin.
	 */
	public IntradbMiscCustomizationsPlugin() {
		logger.info("Configuring IntraDB miscellaneous customizations plugin");
	}
	
}
